from flask import Flask, render_template, redirect, url_for, request
from random import randint
import sqlite3, re
import string, smtplib
app = Flask(__name__)

#ac
firstname = 0
lastname = 1
email = 2
password = 3
gender = 4
age = 5

##permit
P_FIRST = 1
P_LAST = 2
P_DUR = 3
P_TYPE = 4
P_DEPART = 5
P_START = 6
P_END = 7
P_NUMBER = 8
P_EMAIL = 9
P_APPROVE = 10

@app.route('/')
def home():

    return render_template('home.html')

@app.route('/visitor')
def visitor():
    return render_template('visitor.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    error = ''
    if request.method == 'POST':

        con = sqlite3.connect("database.db")
        cur = con.cursor()

        cur.execute("SELECT email FROM AC WHERE email = ?", (request.form['email'],))
        data = cur.fetchall()
        if len(data) == 0:
            con.close()
            error = 'Invalid email or password. Please try again.'
            return render_template('login.html', error=error, email=request.form['email'])

        else:
            con = sqlite3.connect("database.db")
            cur = con.cursor()
            cur.execute("SELECT password FROM AC WHERE email = ?", (request.form['email'],))
            for row in cur:
                if row[0] == request.form['password']:
                    return redirect(url_for('main', email=request.form['email']))
                else:
                    error = 'Invalid email or password. Please try again.'
                    return render_template('login.html', error=error, email=request.form['email'])
                
    return render_template('login.html', error=error)

@app.route('/hs_login', methods=['GET', 'POST'])
def hs_login():
    error = ''
    if request.method == 'POST':

        con = sqlite3.connect("database.db")
        cur = con.cursor()

        cur.execute("SELECT email FROM hsac WHERE email = ?", (request.form['email'],))
        data = cur.fetchall()
        if len(data) == 0:
            con.close()
            error = 'Invalid email or password. Please try again.'
            return render_template('hs_login.html', error=error, email=request.form['email'])

        else:
            con = sqlite3.connect("database.db")
            cur = con.cursor()
            cur.execute("SELECT password FROM hsac WHERE email = ?", (request.form['email'],))
            for row in cur:
                if row[0] == request.form['password']:
                    return redirect(url_for('hs_main', email=request.form['email']))
                else:
                    error = 'Invalid email or password. Please try again.'
                    return render_template('hs_login.html', error=error, email=request.form['email'])
                
    return render_template('hs_login.html', error=error)


@app.route('/forgot', methods=['GET', 'POST'])
def forgot():
    error = ''
    success = ''
    if request.method == 'POST':
        con = sqlite3.connect("database.db")
        cur = con.cursor()

        cur.execute("SELECT email FROM AC WHERE email = ?", (request.form['email'],))
        data = cur.fetchall()
        if len(data) == 0:
            con.close()
            error = 'No this email.'
            return render_template('forgot.html', error=error, email=request.form['email'])
        else:
            cur.execute("SELECT password FROM AC WHERE email = ?", (request.form['email'],))
            for row in cur:   
                sendMail(request.form['email'], row[0])
                success = 'The password is sent to the email successfully.'
                return render_template('forgot.html', success=success, email=request.form['email'])
    return render_template('forgot.html', error=error)

@app.route('/hs_forgot', methods=['GET', 'POST'])
def hs_forgot():
    error = ''
    success = ''
    if request.method == 'POST':
        con = sqlite3.connect("database.db")
        cur = con.cursor()

        cur.execute("SELECT email FROM hsAC WHERE email = ?", (request.form['email'],))
        data = cur.fetchall()
        if len(data) == 0:
            con.close()
            error = 'No this email.'
            return render_template('hs_forgot.html', error=error, email=request.form['email'])
        else:
            cur.execute("SELECT password FROM hsAC WHERE email = ?", (request.form['email'],))
            for row in cur:   
                sendMail(request.form['email'], row[0])
                success = 'The password is sent to the email successfully.'
                return render_template('hs_forgot.html', success=success, email=request.form['email'])
    return render_template('hs_forgot.html', error=error)

def sendMail(email, password):
    From = 'Atmiya College Parking System' 
    To = email
    Subject = 'Forgot password'  
    Body = 'Email: ' + email + '\nPassword: ' + password

    message = string.join((
        "From: Parking System%s" % From,
        "To: %s" % To,
        "Subject: %s" % Subject,
        "",
        Body,
        ), "\r\n")
    
    
    username = 'teamparking84@gmail.com'
    password = 'pwteam84'
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.ehlo()
    server.starttls()
    server.login(username,password)
    server.sendmail(From, To, message)
    server.quit()

@app.route('/permit', methods=['GET', 'POST'])
def permit():
    err = False
    error = ''
    if request.method == 'POST':
        ok = False
        while ( ok == False ):
            number = ''
            for i in range(0, 9):
                number += str(randint(0,9))

            con = sqlite3.connect("database.db")
            cur = con.cursor()

            cur.execute("SELECT number FROM PERMIT WHERE number = ?", (number,))
            data = cur.fetchall()
            if len(data) == 0:
                con.close()
                con = sqlite3.connect("database.db")
                cur = con.cursor()

                cur.execute("SELECT number FROM spermit WHERE number = ?", (number,))
                data = cur.fetchall()
                if len(data) == 0:
                    con.close()
                    ok = True
                else:
                    con.close()
            else:
                con.close()

        con = sqlite3.connect("database.db")
        cur = con.cursor()
        cur.execute("INSERT INTO PERMIT (user, first, last, duration, type, department, start, end, number, email, approve, amount, invoice) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)",('v', request.form['firstname'], request.form['lastname'], request.form['duration'], request.form['type'], request.form['department'], request.form['start'], request.form['end'], number, request.form['email'], 'n', '20', '') )
        con.commit()
        con.close()

        return redirect(url_for('permitted', number = number))

    return render_template('permit.html', error=error)

@app.route('/permitted/<number>', methods=['GET', 'POST'])
def permitted(number):
    success = ''
    if request.method == 'POST':

        con = sqlite3.connect("database.db")
        cur = con.cursor()
        cur.execute("SELECT user, first, last, duration, type, department, start, end, number, email, approve FROM PERMIT WHERE number = ?", (number,))
        for row in cur:
            if row[P_DUR] == 'y':
                duration = 'Yearly'
            if row[P_DUR] == 'm':
                duration = 'Monthly'
            if row[P_DUR] == 'd':
                duration = 'Daily'
            if row[P_DUR] == 'h':
                duration = 'Hourly'

            if row[P_TYPE] == '2':
                type = 'Two Wheeler'
            if row[P_TYPE] == '4':
                type = 'Four Wheeler'
            if row[P_TYPE] == 'o':
                type = 'Other'

            con.close()


            sendDetail(first=row[P_FIRST], last=row[P_LAST], duration=duration, type=type, department=row[P_DEPART], start=row[P_START], end=row[P_END], number=number, email=row[P_EMAIL])
            success = 'The details are sent to the email successfully.'
            return render_template('permitted.html', success=success, first=row[P_FIRST], last=row[P_LAST], duration=duration, type=type, department=row[P_DEPART], start=row[P_START], end=row[P_END], number=number, email=row[P_EMAIL])


    con = sqlite3.connect("database.db")
    cur = con.cursor()
    cur.execute("SELECT user, first, last, duration, type, department, start, end, number, email, approve FROM PERMIT WHERE number = ?", (number,))
    for row in cur:
        if row[P_DUR] == 'y':
            duration = 'Yearly'
        if row[P_DUR] == 'm':
            duration = 'Monthly'
        if row[P_DUR] == 'd':
            duration = 'Daily'
        if row[P_DUR] == 'h':
            duration = 'Hourly'

        if row[P_TYPE] == '2':
            type = 'Two Wheeler'
        if row[P_TYPE] == '4':
            type = 'Four Wheeler'
        if row[P_TYPE] == 'o':
            type = 'Other'
        con.close()


        return render_template('permitted.html', first=row[P_FIRST], last=row[P_LAST], duration=duration, type=type, department=row[P_DEPART], start=row[P_START], end=row[P_END] , number=number, email=row[P_EMAIL])

def sendDetail(first, last, duration, type, department, start, end, number, email):
    From = 'Atmiya College Parking System'
    To = email
    Subject = 'Apply permit'
    Body = 'First Name: '+first +'\nLast Name: '+last+'\nEmail: '+email+'\nDuration: '+duration+'\nVehicle Type: '+type+'\nDepartment: '+department+'\nPermit Start Date: '+start+'\nPermit End Date: '+end+'\nPermit Number: '+number

    message = string.join((
        "From: Parking System%s" % From,
        "To: %s" % To,
        "Subject: %s" % Subject,
        "",
        Body,
        ), "\r\n")


    username = 'teamparking84@gmail.com'
    password = 'pwteam84'
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.ehlo()
    server.starttls()
    server.login(username,password)
    server.sendmail(From, To, message)
    server.quit()


@app.route('/check_status', methods=['GET', 'POST'])
def check_status():
    if request.method == 'POST':
        con = sqlite3.connect("database.db")
        cur = con.cursor()

        cur.execute("SELECT number FROM PERMIT WHERE number = ?", (request.form['number'],))
        data = cur.fetchall()
        if len(data) == 0:
            con.close()
            error = 'No this permit number.'
            return render_template('check_status.html', error=error)
        else:
            con.close()
            return redirect(url_for('permit_status', number = request.form['number']))

    return render_template('check_status.html')

@app.route('/permit_status/<number>')
def permit_status(number):
    pay=''
    con = sqlite3.connect("database.db")
    cur = con.cursor()
    cur.execute("SELECT user, first, last, duration, type, department, start, end, number, email, approve FROM PERMIT WHERE number = ?", (number,))
    for row in cur:
        if row[P_DUR] == 'y':
            duration = 'Yearly'
        if row[P_DUR] == 'm':
            duration = 'Monthly'
        if row[P_DUR] == 'd':
            duration = 'Daily'
        if row[P_DUR] == 'h':
            duration = 'Hourly'

        if row[P_TYPE] == '2':
            type = 'Two Wheeler'
        if row[P_TYPE] == '4':
            type = 'Four Wheeler'
        if row[P_TYPE] == 'o':
            type = 'Other'

        if row[P_APPROVE] == 'n':
            approve = 'Processing'
        if row[P_APPROVE] == 'y':
            approve = 'Approved'
            pay='1'
        if row[P_APPROVE] == 'r':
            approve = 'Rejected'
        if row[P_APPROVE] == 'p':
            approve = 'Paid'
            
        con.close()

        return render_template('permit_status.html', first=row[P_FIRST], last=row[P_LAST], duration=duration, type=type, department=row[P_DEPART], start=row[P_START], end=row[P_END], number=number, email=row[P_EMAIL], approve=approve, pay=pay)



@app.route('/forgot_number', methods=['GET', 'POST'])
def forgot_number():
    error = ''
    success = ''
    if request.method == 'POST':

        con = sqlite3.connect("database.db")
        cur = con.cursor()

        cur.execute("SELECT email FROM PERMIT WHERE email = ?", (request.form['email'],))
        data = cur.fetchall()
        if len(data) == 0:
            con.close()
            error = 'No this email.'
            return render_template('forgot_number.html', error=error, email=request.form['email'])

        else:
            con.close()
            con = sqlite3.connect("database.db")
            cur = con.cursor()
            cur.execute("SELECT user, first, last, duration, type, department, start, end, number, email, approve FROM PERMIT WHERE email = ?", (request.form['email'],))
            for row in cur:
                sendPermit(row[P_NUMBER], row[P_EMAIL])
                success = 'The permit number is sent to the email successfully.'
                return render_template('forgot_number.html', success=success)


    return render_template('forgot_number.html', error=error)

def sendPermit(number, email):

    From = 'Atmiya College Parking System'
    To = email
    Subject = 'Forgot permit number'
    Body = 'Permit number: ' + number

    message = string.join((
        "From: Parking System%s" % From,
        "To: %s" % To,
        "Subject: %s" % Subject,
        "",
        Body,
        ), "\r\n")


    username = 'teamparking84@gmail.com'
    password = 'pwteam84'
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.ehlo()
    server.starttls()
    server.login(username,password)
    server.sendmail(From, To, message)
    server.quit()


@app.route('/parking_rules')    
def parking_rules():
    return render_template('parking_rules.html')

@app.route('/health_safety_rules')    
def health_safety_rules():
    return render_template('health_safety_rules.html')

@app.route('/s_health_safety_rules/<email>')    
def s_health_safety_rules(email):
    con = sqlite3.connect("database.db")
    cur = con.cursor()
    cur.execute("SELECT firstname, lastname FROM AC WHERE email = ?", (email,))
    for row in cur:
        return render_template('s_health_safety_rules.html', first=row[firstname], last=row[lastname], email=email)


@app.route('/hs_health_safety_rules/<email>')    
def hs_health_safety_rules(email):
    con = sqlite3.connect("database.db")
    cur = con.cursor()
    cur.execute("SELECT firstname, lastname FROM hsac WHERE email = ?", (email,))
    for row in cur:
        return render_template('hs_health_safety_rules.html', first=row[firstname], last=row[lastname], email=email)


@app.route('/report_violations/<email>', methods=['GET', 'POST'])    
def report_violations(email):
    if request.method == 'POST': 
        if request.form['issue'] == 'with':
            con = sqlite3.connect("database.db")
            cur = con.cursor()
            cur.execute("INSERT INTO violation (issue, license, type, number, name, department, problem, date) VALUES (?,?,?,?,?,?,?,?)",('with', request.form['license'], request.form['type'], request.form['number'], '', '', request.form['problem1'], request.form['date1']) )
            con.commit()
            con.close()
            return redirect(url_for('hs_main', email=email))

        if request.form['issue'] == 'without':
            con = sqlite3.connect("database.db")
            cur = con.cursor()
            cur.execute("INSERT INTO violation (issue, license, type, number, name, department, problem, date) VALUES (?,?,?,?,?,?,?,?)",('without', request.form['license'], request.form['type'], '', '', '', request.form['problem2'], request.form['date2']) )
            con.commit()
            con.close()
            return redirect(url_for('hs_main', email=email))

        if request.form['issue'] == 'smoke':
            con = sqlite3.connect("database.db")
            cur = con.cursor()
            cur.execute("INSERT INTO violation (issue, license, type, number, name, department, problem, date) VALUES (?,?,?,?,?,?,?,?)",('smoke', '', '', '', request.form['name'], request.form['department'], request.form['problem3'], request.form['date3']) )
            con.commit()
            con.close()
            return redirect(url_for('hs_main', email=email))

    con = sqlite3.connect("database.db")
    cur = con.cursor()
    cur.execute("SELECT firstname, lastname FROM hsac WHERE email = ?", (email,))
    for row in cur:
        return render_template('report_violations.html', first=row[firstname], last=row[lastname], email=email)


@app.route('/main/<email>', methods=['GET', 'POST'])
def main(email):        
    date_l = []
    issue_l = []
    status_l = []
    con = sqlite3.connect("database.db")
    cur = con.cursor()
    cur.execute("SELECT start, approve FROM spermit WHERE email = ?", (email,))
    for row in cur:
        date_l.append(row[0])
        issue_l.append('Parking Permit')
        if row[1] == 'n':
            status_l.append('Processing')
        if row[1] == 'y':
            status_l.append('Approved')
        if row[1] == 'r':
            status_l.append('Rejected')
        if row[1] == 'p':
            status_l.append('Paid')

    con.close()

    x = {'date':date_l, 'issue':issue_l, 'status':status_l}

    read_message=0
    num=0
    setting=""
        
    date_l2=[]
    message_l=[]
    message2_l=[]
    title_l=[]
    title2_l=[]
    notice_type_l=[]
    notice_type2_l=[]

    con = sqlite3.connect("database.db")
    cur = con.cursor()
    cur.execute("SELECT notification, setting FROM AC WHERE email = ?", (email,))
    for row in cur:
        read_message=int(row[0])
        setting=row[1]
    con.close()

    unread = []
    con = sqlite3.connect("database.db")
    cur = con.cursor()
    cur.execute("SELECT date, message, number, title, type FROM notice")
    for row in cur:
        if setting == "both":
            date_l2.append(row[0])
            message_l.append(row[1])
            title_l.append(row[3])
            notice_type_l.append(row[4])
            num = int(row[2])
            if read_message < int(row[2]):
                    unread.append(row[2])
        if setting == "h":
            if row[4] == "h":
                date_l2.append(row[0])
                message_l.append(row[1])
                title_l.append(row[3])
                notice_type_l.append(row[4])
                if read_message < int(row[2]):
                    unread.append(row[2])
                num = int(row[2])
        if setting == "p":
            if row[4] == "p":
                date_l2.append(row[0])
                message_l.append(row[1])
                title_l.append(row[3])
                notice_type_l.append(row[4])
                if read_message < int(row[2]):
                    unread.append(row[2])
                num = int(row[2])
        if setting == "n":
            num = int(row[2])
        message2_l.append(row[1])
        notice_type2_l.append(row[4])
        title2_l.append(row[3])
    con.close()    

    if request.method == 'POST':
        read_message=int(unread[0])
        con = sqlite3.connect("database.db")
        cur = con.cursor()
        cur.execute("UPDATE ac set notification='"+ str(read_message) +"' WHERE email = ?", (email,))
        con.commit()
        con.close()
        return redirect(url_for('main', email=email))

    new_message=""
    title=""
    notice_type=""
    if read_message < num:
        if setting != "n":
            new_message = message2_l[int(unread[0])-1]
            if notice_type2_l[int(unread[0])-1] == "h":
                notice_type = "H&S Rules"
            else:
                notice_type = "Parking Rules"
            title = title2_l[int(unread[0])-1]

    y = {'date2':date_l2, 'message':message_l}    
    
    con = sqlite3.connect("database.db")
    cur = con.cursor()
    cur.execute("SELECT firstname, lastname FROM AC WHERE email = ?", (email,))
    for row in cur:
        return render_template('main.html', first=row[firstname], last=row[lastname], email=email, x=x, y=y, new_message=new_message, title=title, notice_type=notice_type, setting=setting)
    

@app.route('/hs_main/<email>')
def hs_main(email):
    people_l = []
    date_l = []
    issue_l = []
    status_l = []
    con = sqlite3.connect("database.db")
    cur = con.cursor()
    cur.execute("SELECT start, approve FROM spermit")
    for row in cur:
        people_l.append("Student/Staff")
        date_l.append(row[0])
        issue_l.append('Parking Permit')
        if row[1] == 'n':
            status_l.append('Processing')
        if row[1] == 'y':
            status_l.append('Approved')
        if row[1] == 'r':
            status_l.append('Rejected')
        if row[1] == 'p':
            status_l.append('Paid')

    con.close()

    con = sqlite3.connect("database.db")
    cur = con.cursor()
    cur.execute("SELECT start, approve FROM permit")
    for row in cur:
        people_l.append("Visitor")
        date_l.append(row[0])
        issue_l.append('Parking Permit')
        if row[1] == 'n':
            status_l.append('Processing')
        if row[1] == 'y':
            status_l.append('Approved')
        if row[1] == 'r':
            status_l.append('Rejected')
        if row[1] == 'p':
            status_l.append('Paid')

    con.close()

    con = sqlite3.connect("database.db")
    cur = con.cursor()
    cur.execute("SELECT date, problem, issue FROM violation")
    for row in cur:
        if row[2] == 'with':
            people_l.append("With parking permit")
        if row[2] == 'without':
            people_l.append("Without parking permit")
        if row[2] == 'smoke':
            people_l.append("Smoking")
        
        
        date_l.append(row[0])
        issue_l.append('Violation')
        status_l.append(row[1])

    con.close()
    
    x = {'date':date_l, 'issue':issue_l, 'status':status_l, 'people':people_l}

    
    con = sqlite3.connect("database.db")
    cur = con.cursor()
    cur.execute("SELECT firstname, lastname FROM hsac WHERE email = ?", (email,))
    for row in cur:
        
        return render_template('hs_main.html', first=row[firstname], last=row[lastname], email=email, x=x)

    
@app.route('/permit_overview/<email>')
def permit_overview(email):

    number_l=[]
    start_l=[]
    end_l=[]
    type_l=[]
    duration_l=[]
    status_l=[]
    pay_l=[]
    pay=''
    
    con = sqlite3.connect("database.db")
    cur = con.cursor()
    cur.execute("SELECT number, start, end, type, duration, approve FROM spermit WHERE email = ?", (email,))
    for row in cur:
        if row[4] == 'y':
            duration = 'Yearly'
        if row[4] == 'm':
            duration = 'Monthly'

        if row[3] == '2':
            type = 'Two Wheeler'
        if row[3] == '4':
            type = 'Four Wheeler'
        if row[3] == 'o':
            type = 'Other'

        if row[5] == 'n':
            approve = 'Processing'
            pay_l.append('')
        if row[5] == 'y':
            approve = 'Approved'
            pay_l.append('Payment')
            pay='1'       
        if row[5] == 'r':
            approve = 'Rejected'
            pay_l.append('')
        if row[5] == 'p':
            approve = 'Paid'
            pay_l.append('')

            
        number_l.append(row[0])
        start_l.append(row[1])
        end_l.append(row[2])
        type_l.append(type)
        duration_l.append(duration)
        status_l.append(approve)
    
    con.close()
    
    x = {'number_l':number_l, 'start_l':start_l, 'end_l':end_l, 'type_l':type_l, 'duration_l':duration_l, 'status_l':status_l, 'pay_l':pay_l}




    con = sqlite3.connect("database.db")
    cur = con.cursor()
    cur.execute("SELECT firstname, lastname FROM AC WHERE email = ?", (email,))
    for row in cur:
        return render_template('permit_overview.html', first=row[firstname], last=row[lastname], email=email, x=x, pay=pay)


@app.route('/hs_permit_overview/<email>')
def hs_permit_overview(email):

    number_l=[]
    start_l=[]
    end_l=[]
    type_l=[]
    duration_l=[]
    status_l=[]
    email_l=[]
    first_l=[]
    last_l=[]
    department_l=[]
    
    con = sqlite3.connect("database.db")
    cur = con.cursor()
    cur.execute("SELECT number, start, end, type, duration, approve, first, last, email, department FROM spermit")
    for row in cur:
        if row[4] == 'y':
            duration = 'Yearly'
        if row[4] == 'm':
            duration = 'Monthly'

        if row[3] == '2':
            type = 'Two Wheeler'
        if row[3] == '4':
            type = 'Four Wheeler'
        if row[3] == 'o':
            type = 'Other'

        if row[5] == 'n':
            approve = 'Processing'

        if row[5] == 'y':
            approve = 'Approved'
     
        if row[5] == 'r':
            approve = 'Rejected'

        if row[5] == 'p':
            approve = 'Paid'

        first_l.append(row[6])
        last_l.append(row[7])
        email_l.append(row[8])
        department_l.append(row[9])
        number_l.append(row[0])
        start_l.append(row[1])
        end_l.append(row[2])
        type_l.append(type)
        duration_l.append(duration)
        status_l.append(approve)
    
    con.close()

    con = sqlite3.connect("database.db")
    cur = con.cursor()
    cur.execute("SELECT number, start, end, type, duration, approve, first, last, email, department FROM permit")
    for row in cur:
        if row[4] == 'y':
            duration = 'Yearly'
        if row[4] == 'm':
            duration = 'Monthly'

        if row[3] == '2':
            type = 'Two Wheeler'
        if row[3] == '4':
            type = 'Four Wheeler'
        if row[3] == 'o':
            type = 'Other'

        if row[5] == 'n':
            approve = 'Processing'

        if row[5] == 'y':
            approve = 'Approved'
     
        if row[5] == 'r':
            approve = 'Rejected'

        if row[5] == 'p':
            approve = 'Paid'


        first_l.append(row[6])
        last_l.append(row[7])
        email_l.append(row[8])
        department_l.append(row[9])
        number_l.append(row[0])
        start_l.append(row[1])
        end_l.append(row[2])
        type_l.append(type)
        duration_l.append(duration)
        status_l.append(approve)
    
    con.close()
    
    x = {'number_l':number_l, 'start_l':start_l, 'end_l':end_l, 'type_l':type_l, 'duration_l':duration_l, 'status_l':status_l, 'first_l':first_l, 'last_l':last_l, 'email_l':email_l, 'department_l':department_l}




    con = sqlite3.connect("database.db")
    cur = con.cursor()
    cur.execute("SELECT firstname, lastname FROM hsac WHERE email = ?", (email,))
    for row in cur:
        return render_template('hs_permit_overview.html', first=row[firstname], last=row[lastname], email=email, x=x)

@app.route('/approve/<email>/<number>')
def approve(email,number):
    con = sqlite3.connect("database.db")
    cur = con.cursor()
    cur.execute("UPDATE spermit set approve='y' where number='"+number+"'")
    con.commit()
    con.close()

    con = sqlite3.connect("database.db")
    cur = con.cursor()
    cur.execute("UPDATE permit set approve='y' where number='"+number+"'")
    con.commit()
    con.close()
    
    return redirect(url_for('hs_permit_overview', email=email))

@app.route('/reject/<email>/<number>')
def reject(email,number):
    con = sqlite3.connect("database.db")
    cur = con.cursor()
    cur.execute("UPDATE spermit set approve='r' where number='"+number+"'")
    con.commit()
    con.close()

    con = sqlite3.connect("database.db")
    cur = con.cursor()
    cur.execute("UPDATE permit set approve='r' where number='"+number+"'")
    con.commit()
    con.close()
    
    return redirect(url_for('hs_permit_overview', email=email))

@app.route('/delete/<email>/<number>')
def delete(email,number):
    con = sqlite3.connect("database.db")
    cur = con.cursor()
    cur.execute("DELETE FROM spermit WHERE number='"+number+"'")
    con.commit()
    con.close()

    con = sqlite3.connect("database.db")
    cur = con.cursor()
    cur.execute("DELETE FROM permit WHERE number='"+number+"'")
    con.commit()
    con.close()
    
    return redirect(url_for('hs_permit_overview', email=email))

	
@app.route('/permit_apply/<email>', methods=['GET', 'POST'])
def permit_apply(email):
    if request.method == 'POST':
        ok = False
        while ( ok == False ):
            number = ''
            for i in range(0, 9):
                number += str(randint(0,9))

            con = sqlite3.connect("database.db")
            cur = con.cursor()

            cur.execute("SELECT number FROM spermit WHERE number = ?", (number,))
            data = cur.fetchall()
            if len(data) == 0:
                con.close()
                con = sqlite3.connect("database.db")
                cur = con.cursor()

                cur.execute("SELECT number FROM spermit WHERE number = ?", (number,))
                data = cur.fetchall()
                if len(data) == 0:
                    con.close()
                    ok = True
                else:
                    con.close()
            else:
                con.close()

        con = sqlite3.connect("database.db")
        cur = con.cursor()
        cur.execute("SELECT firstname, lastname FROM AC WHERE email = ?", (email,))
        for row in cur:
            first=row[firstname]
            last=row[lastname]
        con.close()
        
        con = sqlite3.connect("database.db")
        cur = con.cursor()
        cur.execute("INSERT INTO spermit (user, first, last, duration, type, department, start, end, number, email, approve, amount, invoice) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)",('s', first, last, request.form['duration'], request.form['type'], request.form['department'], request.form['start'], request.form['end'], number, email, 'n', '20', '') )
        con.commit()
        con.close()

        return redirect(url_for('permit_overview', email=email))

    con = sqlite3.connect("database.db")
    cur = con.cursor()
    cur.execute("SELECT firstname, lastname FROM AC WHERE email = ?", (email,))
    for row in cur:
        return render_template('permit_apply.html', first=row[firstname], last=row[lastname], email=email)

    

@app.route('/payment/<email>/<number>', methods=['GET', 'POST'])
def payment(email,number):
    if request.method == 'POST':      
        
        return redirect(url_for('confirm',email=email, number=number, name=request.form['name'], cardnum=request.form['cardnum'], monthyear=request.form['monthyear'], ccv=request.form['ccv'] ))

    con = sqlite3.connect("database.db")
    cur = con.cursor()
    cur.execute("SELECT amount FROM spermit WHERE number = ?", (number,))
    for row in cur:
        amount = '$'+row[0]
    con.close()    
    
    con = sqlite3.connect("database.db")
    cur = con.cursor()
    cur.execute("SELECT firstname, lastname FROM AC WHERE email = ?", (email,))
    for row in cur:
        return render_template('payment.html', first=row[firstname], last=row[lastname], email=email, number=number, amount=amount)

@app.route('/confirm/<email>/<number>/<name>/<cardnum>/<monthyear>/<ccv>', methods=['GET', 'POST'])
def confirm(email,number,name,cardnum,monthyear,ccv):
    if request.method == 'POST':
        ok = False
        while ( ok == False ):
            invoice = ''
            for i in range(0, 16):
                invoice += str(randint(0,16))

            con = sqlite3.connect("database.db")
            cur = con.cursor()

            cur.execute("SELECT invoice FROM spermit WHERE invoice = ?", (invoice,))
            data = cur.fetchall()
            if len(data) == 0:
                con.close()
                ok = True
            else:
                con.close()

        con = sqlite3.connect("database.db")
        cur = con.cursor()
        cur.execute("UPDATE spermit set invoice='"+invoice+"' where number='"+number+"'")
        cur.execute("UPDATE spermit set approve='p' where number='"+number+"'")
        con.commit()
        con.close()
        return redirect(url_for('receipt', email=email, number=number, name=name, cardnum=cardnum))

    con = sqlite3.connect("database.db")
    cur = con.cursor()
    cur.execute("SELECT first, last, amount, invoice FROM spermit WHERE number = ?", (number,))
    for row in cur:
        first=row[0]
        last=row[1]
        amount=row[2]
        invoice=row[3]
    con.close()

    
    return render_template('confirm.html', first=first, last=last, amount=amount, number=number, email=email, invoice=invoice, name=name, cardnum=cardnum, monthyear=monthyear, ccv=ccv)


@app.route('/receipt/<email>/<number>/<name>/<cardnum>', methods=['GET', 'POST'])
def receipt(email,number,name,cardnum):
    card_l=cardnum
    card_l.split ()
    card = card_l[0] + card_l[1] + card_l[2] + card_l[3] + ' **** **** ' + card_l[12] + card_l[13] + card_l[14] + card_l[15]
        
    success = ''
    if request.method == 'POST':
        sendReceipt(number, email)
        success = 'The receipt is sent to the email successfully.'
        con = sqlite3.connect("database.db")
        cur = con.cursor()
        cur.execute("SELECT first, last, amount, invoice FROM spermit WHERE number = ?", (number,))
        for row in cur:
            amount = '$'+row[2]
            return render_template('receipt.html', first=row[firstname], last=row[lastname], email=email, number=number, amount=amount, invoice=row[3], success=success, name=name, cardnum=card)
    
    con = sqlite3.connect("database.db")
    cur = con.cursor()
    cur.execute("SELECT first, last, amount, invoice FROM spermit WHERE number = ?", (number,))
    for row in cur:
        amount = '$'+row[2]
        return render_template('receipt.html', first=row[firstname], last=row[lastname], email=email, number=number, amount=amount, invoice=row[3], name=name, cardnum=card)


def sendReceipt(number, email):
    con = sqlite3.connect("database.db")
    cur = con.cursor()
    cur.execute("SELECT first, last, amount, invoice FROM spermit WHERE number = ?", (number,))
    for row in cur:
        first=row[0]
        last=row[1]
        amount=row[2]
        invoice=row[3]
    con.close() 
    
    From = 'Atmiya College Parking System'
    To = email
    Subject = 'Payment Receipt'
    Body = 'First Name: ' + first + '\nLast Name: ' + last + '\nEmail: ' + email + '\nPermit Number: ' + number + '\nAmount Due: $' + amount + '\nInvoice Number: ' + invoice

    message = string.join((
        "From: Parking System%s" % From,
        "To: %s" % To,
        "Subject: %s" % Subject,
        "",
        Body,
        ), "\r\n")


    username = 'teamparking84@gmail.com'
    password = 'pwteam84'
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.ehlo()
    server.starttls()
    server.login(username,password)
    server.sendmail(From, To, message)
    server.quit()

@app.route('/v_payment/<number>', methods=['GET', 'POST'])
def v_payment(number):
    if request.method == 'POST':
        
        return redirect(url_for('v_confirm', number=number, name=request.form['name'], cardnum=request.form['cardnum'], monthyear=request.form['monthyear'], ccv=request.form['ccv'] ))


    con = sqlite3.connect("database.db")
    cur = con.cursor()
    cur.execute("SELECT first, last, email, amount FROM permit WHERE number = ?", (number,))
    for row in cur:
        return render_template('v_payment.html', first=row[firstname], last=row[lastname], email=row[2], number=number, amount=row[3])


@app.route('/v_confirm/<number>/<name>/<cardnum>/<monthyear>/<ccv>', methods=['GET', 'POST'])
def v_confirm(number,name,cardnum,monthyear,ccv):
    if request.method == 'POST':
        ok = False
        while ( ok == False ):
            invoice = ''
            for i in range(0, 16):
                invoice += str(randint(0,16))

            con = sqlite3.connect("database.db")
            cur = con.cursor()

            cur.execute("SELECT invoice FROM permit WHERE invoice = ?", (invoice,))
            data = cur.fetchall()
            if len(data) == 0:
                con.close()
                ok = True
            else:
                con.close()

        con = sqlite3.connect("database.db")
        cur = con.cursor()
        cur.execute("UPDATE permit set invoice='"+invoice+"' where number='"+number+"'")
        cur.execute("UPDATE permit set approve='p' where number='"+number+"'")
        con.commit()
        con.close()
        return redirect(url_for('v_receipt', number=number, name=name, cardnum=cardnum))

    con = sqlite3.connect("database.db")
    cur = con.cursor()
    cur.execute("SELECT first, last, amount, invoice, email FROM permit WHERE number = ?", (number,))
    for row in cur:
        first=row[0]
        last=row[1]
        amount=row[2]
        invoice=row[3]
        email=row[4]
    con.close()

    
    return render_template('v_confirm.html', first=first, last=last, amount=amount, number=number, email=email, invoice=invoice, name=name, cardnum=cardnum, monthyear=monthyear, ccv=ccv)


@app.route('/v_receipt/<number>/<name>/<cardnum>', methods=['GET', 'POST'])
def v_receipt(number,name,cardnum):
    card_l=cardnum
    card_l.split ()
    card = card_l[0] + card_l[1] + card_l[2] + card_l[3] + ' **** **** ' + card_l[12] + card_l[13] + card_l[14] + card_l[15]
    success = ''
    if request.method == 'POST':
        con = sqlite3.connect("database.db")
        cur = con.cursor()
        cur.execute("SELECT first, last, amount, invoice, email FROM permit WHERE number = ?", (number,))
        for row in cur:
            amount = '$'+row[2]
            email = row[4]
            sendV_Receipt(number, email)
            success = 'The receipt is sent to the email successfully.'
            return render_template('v_receipt.html', first=row[firstname], last=row[lastname], email=email, number=number, amount=amount, invoice=row[3], success=success, name=name, cardnum=card)
    
    con = sqlite3.connect("database.db")
    cur = con.cursor()
    cur.execute("SELECT first, last, amount, invoice, email FROM permit WHERE number = ?", (number,))
    for row in cur:
        amount = '$'+row[2]
        email = row[4]
        return render_template('v_receipt.html', first=row[firstname], last=row[lastname], email=email, number=number, amount=amount, invoice=row[3], name=name, cardnum=card)

def sendV_Receipt(number, email):
    con = sqlite3.connect("database.db")
    cur = con.cursor()
    cur.execute("SELECT first, last, amount, invoice FROM permit WHERE number = ?", (number,))
    for row in cur:
        first=row[0]
        last=row[1]
        amount=row[2]
        invoice=row[3]
    con.close() 
    
    From = 'Atmiya College Parking System'
    To = email
    Subject = 'Payment Receipt'
    Body = 'First Name: ' + first + '\nLast Name: ' + last + '\nEmail: ' + email + '\nPermit Number: ' + number + '\nAmount Due: $' + amount + '\nInvoice Number: ' + invoice

    message = string.join((
        "From: Parking System%s" % From,
        "To: %s" % To,
        "Subject: %s" % Subject,
        "",
        Body,
        ), "\r\n")


    username = 'teamparking84@gmail.com'
    password = 'pwteam84'
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.ehlo()
    server.starttls()
    server.login(username,password)
    server.sendmail(From, To, message)
    server.quit()


@app.route('/notification/<email>', methods=['GET', 'POST'])
def notification(email):
    success=""
    if request.method == 'POST':
        num = 0
        numstr = ''
        con = sqlite3.connect("database.db")
        cur = con.cursor()
        cur.execute("SELECT number FROM notice")
        for row in cur:
            num = int(row[0])
            
        num += 1
        numstr = str(num)
        con = sqlite3.connect("database.db")
        cur = con.cursor()
        cur.execute("INSERT INTO notice (number, date, message, title, type) VALUES (?,?,?,?,?)",(numstr, request.form['date'], request.form['message'], request.form['title'], request.form['type']) )
        con.commit()
        con.close()
        success = "y"

        
    con = sqlite3.connect("database.db")
    cur = con.cursor()
    cur.execute("SELECT firstname, lastname FROM hsac WHERE email = ?", (email,))
    for row in cur:
        
        return render_template('notification.html', first=row[firstname], last=row[lastname], email=email, success=success)
    

@app.route('/notifpreferences/<email>', methods=['GET', 'POST'])
def notifpreferences(email):
    h = ""
    p = ""
    setting = ""
    success = ''
    if request.method == 'POST':
        if request.form['h'] == 'h' and request.form['p'] == 'p':
            setting = "both"
        if request.form['h'] == 'h' and request.form['p'] == 'n':
            setting = 'h'
        if request.form['h'] == 'n' and request.form['p'] == 'p':
            setting = 'p'
        if request.form['h'] == 'n' and request.form['p'] == 'n':
            setting = 'n'

        if setting == 'both':
            con = sqlite3.connect("database.db")
            cur = con.cursor()
            cur.execute("UPDATE ac set setting='both' WHERE email = ?", (email,))
            con.commit()
            con.close()
        if setting == 'h':
            con = sqlite3.connect("database.db")
            cur = con.cursor()
            cur.execute("UPDATE ac set setting='h' WHERE email = ?", (email,))
            con.commit()
            con.close()
        if setting == 'p':
            con = sqlite3.connect("database.db")
            cur = con.cursor()
            cur.execute("UPDATE ac set setting='p' WHERE email = ?", (email,))
            con.commit()
            con.close()
        if setting == 'n':
            con = sqlite3.connect("database.db")
            cur = con.cursor()
            cur.execute("UPDATE ac set setting='n' WHERE email = ?", (email,))
            con.commit()
            con.close()
        success="y"
    
    con = sqlite3.connect("database.db")
    cur = con.cursor()
    cur.execute("SELECT firstname, lastname, setting FROM ac WHERE email = ?", (email,))
    for row in cur:
        if row[2] == "both":
            h = "y"
            p = "y"
        if row[2] == "h":
            h = "y"
        if row[2] == "p":
            p = "y"
        return render_template('notifpreferences.html', first=row[firstname], last=row[lastname], email=email, p=p, h=h, success=success)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=80)

    
##import urllib
##login_data=urllib.urlencode({'username':'admin','password':'admin','submit':'Login'}) # replace username and password with filed name
##op = urllib.urlopen('www.exmaple.com/sign-in.php',login_data)
