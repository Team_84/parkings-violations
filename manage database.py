import sqlite3

def create_table(table, rows):
    conn = sqlite3.connect('database.db')
    print "Opened database successfully";

    text = ''
    
    if len(rows) != 1:
        for i in range(0, (len(rows)-1)):
            text += rows[i]
            text += ' TEXT,'

    text += rows[len(rows)-1]
    text += ' TEXT'
        
    conn.execute('CREATE TABLE ' + str(table) + '(' +text+ ')')
    print "Table created successfully";
    conn.close()
    

def confirm(user_input):
    print 'Confirm \'' + str(user_input) + '\'? (y/n)'
    key = raw_input()
    if key == 'y' or key == 'Y':
        return True

    elif key == 'n' or key == 'N':
        return False
    else:
        confirm(user_input)
    

def create():
    print 'Enter table name:'
    user_input = raw_input()
    
    conf = confirm(user_input)    
    while conf == False:
        print 'Enter table name:'
        user_input = raw_input()
        conf = confirm(user_input)

    table = user_input

    conf = False
    while conf == False:
        print 'Enter number of row:'
        try:
            conf = True
            user_input = int(raw_input())
            
        except ValueError:
            conf = False
            print('you must enter an integer')

    conf = confirm(user_input)    
    while conf == False:
        print 'Enter number of row:'
        user_input = raw_input()
        conf = confirm(user_input)

    number = int(user_input)
    rows = []

    for i in range(0,number):
        print 'Enter row' + str(i+1) + ' name:'
        user_input = raw_input()
        
        conf = confirm(user_input)    
        while conf == False:
            print 'Enter row' + str(i+1) + ' name:'
            user_input = raw_input()
            conf = confirm(user_input)

        rows.append(user_input)

    print 'Table: ' + table
    print 'Rows: ', rows[0:number]
    print 'Create? (y/n)'
    key = ''
    while key != 'y' or key != 'Y' or key != 'n' or key != 'N':
        key = raw_input()
        if key == 'y' or key == 'Y':
            create_table(table, rows)
        elif key == 'n' or key == 'N':
            selection()

def delete_row():
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    cur.execute("SELECT name FROM sqlite_master WHERE type='table'") 
    print cur.fetchall()
    conn.close()

    print 'Enter table name:'
    table = raw_input()

    try:
        
        conn = sqlite3.connect('database.db')

        cur = conn.cursor()
        cur.execute("SELECT * from "+ table)

        for row in cur:
            print row

        conn.close()
        
        try:
            print 'Permit number(n) or email(e):'
            key = raw_input()
            if key == 'n' or key == 'N':
                print 'Enter permit number:'
                number = raw_input()
                conn = sqlite3.connect('database.db')

                cur = conn.cursor()
                conn.execute("DELETE FROM "+table+" WHERE number='"+number+"'")
                conn.commit()
                print "Row " + table + " deleted successfully"
                conn.close()

            elif key == 'e' or key == 'E':
                print 'Enter email:'
                email = raw_input()
                conn = sqlite3.connect('database.db')

                cur = conn.cursor()
                conn.execute("DELETE FROM "+table+" WHERE email='"+email+"'")
                conn.commit()
                print "Row " + table + " deleted successfully"
                conn.close()
                
            else:
                delete_row()
 
        except:
            print 'Please enter correct number.'
            delete_row()

    except:
        print 'Please enter correct name.'
        delete_row()
    

def delete_table():
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    cur.execute("SELECT name FROM sqlite_master WHERE type='table'") 
    print cur.fetchall()
    conn.close()
    
    print 'Enter table name to delete'
    table = raw_input()

    try:
        conn = sqlite3.connect('database.db')
        cur = conn.cursor()
        cur.execute("DROP TABLE " + table)
        print "Table " + table + " deleted successfully"
        conn.close()
    
    except:
        print 'Please enter correct name.'
        delete_table()

    
def delete():
    print 'Table(t) / Row in table(r) / Previous(p)'
    key = raw_input()
    if key == 't' or key == 'T':
        delete_table()

    elif key == 'r' or key == 'R':
        delete_row()

    elif key == 'p' or key == 'P':
        modify()
            
    else:
        delete()
    

def modify():
    print 'Insert(i) / Update(u) / Delete(d) / Previous(p)'
    key = raw_input()

    if key == 'i' or key == 'I':
        print 'i'

    elif key == 'u' or key == 'U':
        print 'u'

    elif key == 'd' or key == 'D':
        delete()

    elif key == 'p' or key == 'P':
        selection()
            
    else:
        modify()

def rows_of_tables():
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    cur.execute("SELECT name FROM sqlite_master WHERE type='table'") 
    print cur.fetchall()
    conn.close()

    print 'Enter table name:'
    table = raw_input()

    try:
        
        conn = sqlite3.connect('database.db')

        cur = conn.cursor()
        
        cur.execute("SELECT * from "+ table)
        for row in cur:
            print row

        conn.close()

    except:
        print 'Please enter correct name.'
        rows_of_tables()

def name_of_colums():
    conn = sqlite3.connect('database.db')
    cur = conn.cursor()
    cur.execute("SELECT name FROM sqlite_master WHERE type='table'") 
    print cur.fetchall()
    conn.close()

    print 'Enter table name:'
    table = raw_input()

    try:
        
        conn = sqlite3.connect('database.db')

        cur = conn.cursor()
        cur.execute('PRAGMA table_info(\''+table+'\')')

        for row in cur:
            print row

        conn.close()

    except:
        print 'Please enter correct name.'
        name_of_colums()

def view():
    print 'Tables(t) / Name of Columns(n) / Rows of Tables(r) / Previous(p)'
    key = raw_input()
    if key == 't' or key == 'T':    
        conn = sqlite3.connect('database.db')
        cur = conn.cursor()
        cur.execute("SELECT name FROM sqlite_master WHERE type='table'")
        
        print cur.fetchall()

        conn.close()

    elif key == 'n' or key == 'N':
        name_of_colums()

    elif key == 'r' or key == 'R':
        rows_of_tables()
        
    elif key == 'p' or key == 'P':
        selection()
        
    else:
        view()

def selection():
    print 'Create(c) / View(v) / Modify(m)'
    key = raw_input()
    
    if key == 'c' or key == 'C':
        create()

    elif key == 'v' or key == 'V':
        view()        
        

    elif key == 'm' or key == 'M':
        modify()

    else:
        selection()
        


while(1):
    selection()

## Created by Matthew Luk
## Version 1.1
